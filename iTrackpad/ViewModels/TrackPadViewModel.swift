//
//  TrackPadViewModel.swift
//  iTrackpad
//
//  Created by Michael Willer on 05/04/2021.
//

import Foundation
import MultipeerConnectivity
import Combine

class TrackPadViewModel: NSObject {
    
    /// A list of discovered devices using the MacOSApp
    public var discoveredDevices: [MCPeerID] = []
    
    private var mcPeerID = MCPeerID(displayName: UIDevice.current.name)
    private var mcSession: MCSession!
    private var browser: MCNearbyServiceBrowser!
    private var advertiser: MCNearbyServiceAdvertiser!
    private var isConnected = false
    
    override init() {
        super.init()
        mcSession = MCSession(peer: mcPeerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession?.delegate = self
        browser = MCNearbyServiceBrowser(peer: mcPeerID, serviceType: "itp-service")
        browser.delegate = self
        advertiser = MCNearbyServiceAdvertiser(peer: mcPeerID, discoveryInfo: nil, serviceType: "itp-service")
        advertiser.delegate = self
        browser.startBrowsingForPeers()
    }
    
    /// Creates a message for each possible gesture and sends it to the host
    public func createMessage(for gesture: Gesture, with data: Any? = nil) {
        guard isConnected == true else { return }
        switch gesture {
        case .tap:
            let message = Message(gesture: .tap)
            send(data: message.pack())
        case .pan:
            var panData = Data()
            if let messageData = data as? [Float] {
                let xCoordinate = messageData[0].data
                panData.append(xCoordinate)
                let yCoordinate = messageData[1].data
                panData.append(yCoordinate)
                let message = Message(gesture: .pan, payload: panData)
                send(data: message.pack())
            }
        case .rightClick:
            let message = Message(gesture: .rightClick)
            send(data: message.pack())
        case .scroll:
            guard let direction = data as? UInt8 else { return }
            var messageData = Data()
            messageData.append(direction)
            let message = Message(gesture: .scroll, payload: messageData)
            send(data: message.pack())
        }
    }
    
    private func send(data: Data) {
        guard let mcSession = mcSession else { return }
        let peers = mcSession.connectedPeers
        do {
            try mcSession.send(data, toPeers: peers, with: .unreliable)
            print("data sent")
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK - MCSession and MCBrowser Delegate methods
extension TrackPadViewModel: MCSessionDelegate, MCNearbyServiceBrowserDelegate, MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        if let data = context?.integer {
            if data == 2410 {
                invitationHandler(true, mcSession)
            }
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        if (info?["iTackpadDiscoveryInfo"]) != nil {
            discoveredDevices.append(peerID)
            let data: Int = 2410
            browser.invitePeer(peerID, to: mcSession, withContext: data.data, timeout: 20)
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        for (index, peer) in discoveredDevices.enumerated() {
            if peer == peerID {
                discoveredDevices.remove(at: index)
            }
        }
        print("lost peer \(peerID)")
    }
    
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        if state == .connected {
            print("Connected to session with \(peerID)")
            isConnected = true
        } else if state == .connecting {
            print("Connecting to session with \(peerID)")
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        print("Recieved data from \(peerID)")
    }
    
    // Empty delegte calls
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) { }
}
