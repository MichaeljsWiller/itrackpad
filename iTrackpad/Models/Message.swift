//
//  Message.swift
//  iTrackpad
//
//  Created by Michael Willer on 05/04/2021.
//

import Foundation
import Combine

public class Message {

    /// The gesture that has been performed
    public let gesture: Gesture
    /// the data to be sent with the gesture
    public let payload: Any?
    
    
    /// Initialises a new message
    /// - Parameters:
    ///   - gesture: The gesture the message represents
    ///   - payload: The data to be sent along with the gesture
    init(gesture: Gesture, payload: Any? = nil) {
        self.gesture = gesture
        self.payload = payload
    }
    
    public func pack() -> Data {
        var packedData = Data()
        // The start message Number
        let startMessageNo: UInt8 = 0x02
        packedData.append(startMessageNo)
        // The gesture
        let gestureData = gesture.rawValue
        packedData.append(gestureData)
        // The payload data
        if let payloadData = payload as? Data {
            packedData.append(payloadData)
        }
        let endMessageNo: UInt8 = 0x03
        packedData.append(endMessageNo)
        return packedData
    }
}

/// Enum representing the possible gestures
public enum Gesture: UInt8 {
    case tap = 0x01
    case pan = 0x02
    case rightClick = 0x03
    case scroll = 0x04
}

public enum Direction: UInt8 {
    case up = 0x01
    case down = 0x02
}
