//
//  ViewController.swift
//  iTrackpad
//
//  Created by Michael Willer on 05/04/2021.
//

import UIKit
import SnapKit

class TrackPadView: UIViewController {
    let viewModel = TrackPadViewModel()
    let backgroundImage = UIImageView()
    let logo = UIImageView()
    let titleImage = UIImageView()
    var panGesture = UIPanGestureRecognizer()
    var tapGesture = UITapGestureRecognizer()
    var rightClick = UITapGestureRecognizer()
    var scrollGesture = UIPanGestureRecognizer()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        setupGestures()
    }
    
    private func setupViews() {
        backgroundImage.image = UIImage(named: "background")
        view.addSubview(backgroundImage)
        logo.image = UIImage(named: "logo")
        view.addSubview(logo)
        titleImage.image = UIImage(named: "title")
        view.addSubview(titleImage)
    }

    private func setupConstraints() {
        backgroundImage.snp.makeConstraints { make in
            make.bottom.top.leading.trailing.equalToSuperview()
        }
        logo.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        titleImage.snp.makeConstraints { make in
            make.centerX.equalTo(logo)
            make.centerY.equalTo(logo).dividedBy(1.6)
        }
    }
}

// MARK - UIGesture Delegate methods
extension TrackPadView: UIGestureRecognizerDelegate {
    
    private func setupGestures() {
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan))
        view.addGestureRecognizer(panGesture)
        scrollGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan))
        scrollGesture.maximumNumberOfTouches = 2
        view.addGestureRecognizer(scrollGesture)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
        view.addGestureRecognizer(tapGesture)
        rightClick = UITapGestureRecognizer(target: self, action: #selector(didTap))
        rightClick.numberOfTouchesRequired = 2
        view.addGestureRecognizer(rightClick)
    }
    
    /// Handles pan gestures
    @objc private func didPan(sender: UIPanGestureRecognizer) {
        // scroll
        if sender.numberOfTouches == 2 {
            let velocity = sender.velocity(in: view)
            let direction: UInt8
            if (velocity.y > velocity.x) == true {
                direction = 0x01
                viewModel.createMessage(for: .scroll, with: direction)
                print("scrolling: up, Velocity: \(direction)")
            } else {
                direction = 0x02
                viewModel.createMessage(for: .scroll, with: direction)
                print("scrolling: down, Velocity: \(direction)")
            }
            // pan
        } else {
            let velocity = sender.velocity(in: view)
            let translation = sender.translation(in: view)
            print("X: \(translation.x), Y: \(translation.y)")
            let xCoordinate = Float(translation.x)
            let yCoordinate = Float(translation.y)
            viewModel.createMessage(for: .pan, with: [xCoordinate, yCoordinate])
        }
    }
    
    /// Handles tap gestures
    @objc private func didTap(sender: UITapGestureRecognizer) {
        // right click
        if sender.numberOfTouchesRequired == 2 {
            if sender.state == .ended {
                viewModel.createMessage(for: .rightClick)
                print("Left Tap Ended")
            }
            // tap
        } else {
            if sender.state == .ended {
                viewModel.createMessage(for: .tap)
                print("Right Tap Ended")
                
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.panGesture && otherGestureRecognizer == self.scrollGesture {
            return true
        }
        return false
    }
}
