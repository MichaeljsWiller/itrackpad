//
//  DataConversion.swift
//  iTrackpadMacOS
//
//  Created by Michael Willer on 06/04/2021.
//

import Foundation

extension Numeric {
    init<D: DataProtocol>(_ data: D) {
            var value: Self = .zero
            let size = withUnsafeMutableBytes(of: &value, { data.copyBytes(to: $0)} )
            assert(size == MemoryLayout.size(ofValue: value))
            self = value
        }
    
    var data: Data {
        var source = self
        // This will return 1 byte for 8-bit, 2 bytes for 16-bit, 4 bytes for 32-bit and 8 bytes for 64-bit binary integers. For floating point types it will return 4 bytes for single-precision, 8 bytes for double-precision and 16 bytes for extended precision.
        return .init(bytes: &source, count: MemoryLayout<Self>.size)
    }
}

extension DataProtocol {
    var integer: Int { value() }
    var int32: Int32 { value() }
    var float: Float { value() }
    var cgFloat: CGFloat { value() }
    var double: Double { value() }
    var decimal: Decimal { value() }

    func value<N: Numeric>() -> N { .init(self) }
}
