//
//  Message.swift
//  iTrackpadMacOS
//
//  Created by Michael Willer on 06/04/2021.
//

import Foundation
public class Message {

    /// The gesture that has been performed
    public let gesture: Gesture
    /// the xCoordinate for a pan gesture
    public let xCoordinate: Float?
    /// The yCoordinate of a pan gesture
    public let yCoordinate: Float?
    /// The direction of a scoll gesture if one is being made
    public let direction: Direction?
    
    
    /// Initialises a new message
    /// - Parameters:
    ///   - gesture: The gesture the message represents
    init(gesture: Gesture, xCoordinate: Float? = nil, yCoordinate: Float? = nil, direction: Direction? = nil) {
        self.gesture = gesture
        self.xCoordinate = xCoordinate
        self.yCoordinate = yCoordinate
        self.direction = direction
    }
}

/// Enum representing the possible gestures
public enum Gesture: UInt8 {
    case tap = 0x01
    case pan = 0x02
    case rightClick = 0x03
    case scroll = 0x04
}

public enum Direction: UInt8 {
    case up = 0x01
    case down = 0x02
}

public extension Data {
    func unpack() -> Message? {
        guard let unpackedGesture = Gesture(rawValue: self[1]) else { return nil }
        var unpackedXData: Float? = nil
        var unpackedYData: Float? = nil
        var unpackedDirection: Direction? = nil
        
        switch unpackedGesture {
        case .tap:
            unpackedXData = nil
            unpackedYData = nil
        case .pan:
            unpackedXData = self[2..<6].float
            unpackedYData = self[6...].float
        case .rightClick:
            unpackedXData = nil
            unpackedYData = nil
        case .scroll:
            if let directionData = Direction(rawValue: self[2]) {
                unpackedDirection = directionData
            }
        }
        return Message(gesture: unpackedGesture, xCoordinate: unpackedXData, yCoordinate: unpackedYData, direction: unpackedDirection)
    }
}

