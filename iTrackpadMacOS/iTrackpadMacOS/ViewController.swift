//
//  ViewController.swift
//  iTrackpadMacOS
//
//  Created by Michael Willer on 06/04/2021.
//

import Cocoa

class ViewController: NSViewController {
    @IBOutlet weak var titleLable: NSTextField!
    @IBOutlet weak var infoLabel: NSTextField!
    @IBOutlet weak var connectionButton: NSButton!
    private let advertiser = AdvertisingViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func advertise(_ sender: NSButton) {
        print("This works, state: \(sender.state)")
        advertiser.startAdvertising()
        sender.title = "Advertising..."
        sender.alternateTitle = "sdfsdf"
    }
}

