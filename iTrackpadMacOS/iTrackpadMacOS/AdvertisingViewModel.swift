//
//  AdvertisingViewModel.swift
//  iTrackpadMacOS
//
//  Created by Michael Willer on 06/04/2021.
//

import Foundation
import MultipeerConnectivity
import Combine

class AdvertisingViewModel: NSObject {
    
    /// A list of discovered devices using the MacOSApp
    public var discoveredDevices: [MCPeerID] = []
    
    private var mcPeerID = MCPeerID(displayName: Host.current().name ?? "Users Mac")
    private var mcSession: MCSession!
    private var advertiser: MCNearbyServiceAdvertiser!
    private var discoveryInfo: [String: String] = ["iTackpadDiscoveryInfo": "iTackpadDiscoveryInfo"]
    private var gestureHandler = GestureHandler()
    var isAdvertising: Bool = false
    
    override init() {
        super.init()
        mcSession = MCSession(peer: mcPeerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession?.delegate = self
        advertiser = MCNearbyServiceAdvertiser(peer: mcPeerID, discoveryInfo: discoveryInfo, serviceType: "itp-service")
        advertiser.delegate = self
    }
    
    /// Creates a message for each possible gesture and sends it to the host
    public func unpackRecieved(data: Data) {
        let unpackedData = data.unpack()
        guard let message = unpackedData else { return }
        switch message.gesture {
        case .tap:
            gestureHandler.recievedTap()
        case .pan:
            gestureHandler.recievedPan(message: message)
        case .rightClick:
            gestureHandler.recievedRightClick()
        case .scroll:
            gestureHandler.recievedScroll(message: message)
        }
        gestureHandler.message = message
        print("gesture: \(unpackedData?.gesture), xco: \(unpackedData?.xCoordinate), yco: \(unpackedData?.yCoordinate), direction: \(message.direction?.rawValue)")
    }
    
    public func startAdvertising() {
        advertiser.startAdvertisingPeer()
        print("started advertising with peerID \(mcPeerID)")
        isAdvertising = true
    }
    
    private func send(data: Data) {
        guard let mcSession = mcSession else { return }
        let peers = mcSession.connectedPeers
        do {
            try mcSession.send(data, toPeers: peers, with: .reliable)
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK - MCSession and MCBrowser Delegate methods
extension AdvertisingViewModel: MCSessionDelegate, MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        if let data = context?.integer {
            if data == 2410 {
                invitationHandler(true, mcSession)
            }
        }
    }
    
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        if state == .connected {
            print("Connected to session with \(peerID)")
        } else if state == .connecting {
            print("Connecting to session with \(peerID)")
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        unpackRecieved(data: data)
        print("Recieved data from \(peerID)")
    }
    
    // Empty delegte calls
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) { }
}
