//
//  GestureHandler.swift
//  iTrackpadMacOS
//
//  Created by Michael Willer on 08/04/2021.
//

import Foundation
import MultipeerConnectivity
import Combine

class GestureHandler {
    
    @Published var rightClicked = false
    private var cancellables: [AnyCancellable] = []
    public var message: Message?
    
    
    func recievedTap() {
        var mouseLoc = NSEvent.mouseLocation
        mouseLoc.y = abs(NSHeight(NSScreen.screens[0].frame) - mouseLoc.y)
        let mouseDown = CGEvent(mouseEventSource: nil,
                                mouseType: .leftMouseDown,
                                mouseCursorPosition: CGPoint(x: mouseLoc.x, y: mouseLoc.y),
                                mouseButton: .left)
        mouseDown?.post(tap: .cghidEventTap)
        rightClicked = false
        
        let mouseUp = CGEvent(mouseEventSource: nil,
                                mouseType: .leftMouseUp,
                                mouseCursorPosition: CGPoint(x: mouseLoc.x, y: mouseLoc.y),
                                mouseButton: .left)
        mouseUp?.post(tap: .cghidEventTap)
        mouseDown?.post(tap: .cghidEventTap)
        mouseUp?.post(tap: .cghidEventTap)
    }
    
    func recievedPan(message: Message) {
        var mouseLoc = NSEvent.mouseLocation
        mouseLoc.y = abs(NSHeight(NSScreen.screens[0].frame) - mouseLoc.y)
        let newLoc = CGPoint(x: mouseLoc.x+CGFloat( message.xCoordinate! / 10), y: mouseLoc.y+CGFloat(message.yCoordinate! / 10))
        for x in stride(from: mouseLoc.x, to: newLoc.y, by: +1 as CGFloat) {
            for y in stride(from: mouseLoc.y, to: newLoc.y, by: +1 as CGFloat) {
                let nextLoc = CGPoint(x: mouseLoc.x+CGFloat( x), y: mouseLoc.y+CGFloat(y))
                CGDisplayMoveCursorToPoint(CGMainDisplayID(), nextLoc)
                rightClicked = false
            }
        }
        CGDisplayMoveCursorToPoint(CGMainDisplayID(), newLoc)
        rightClicked = false
    }
    
    func recievedRightClick() {
        var mouseLoc = NSEvent.mouseLocation
        mouseLoc.y = abs(NSHeight(NSScreen.screens[0].frame) - mouseLoc.y)
        let mouseDown = CGEvent(mouseEventSource: nil,
                                mouseType: .rightMouseDown,
                                mouseCursorPosition: CGPoint(x: mouseLoc.x, y: mouseLoc.y),
                                mouseButton: .right)
        let mouseUp = CGEvent(mouseEventSource: nil,
                                mouseType: .rightMouseUp,
                                mouseCursorPosition: CGPoint(x: mouseLoc.x, y: mouseLoc.y),
                                mouseButton: .right)
        mouseDown?.post(tap: .cghidEventTap)
        rightClicked = true
        
        $rightClicked
            .receive(on: DispatchQueue.main)
            .sink { unclicked in
            if !unclicked && self.message?.gesture != .rightClick {
                mouseUp?.post(tap: .cghidEventTap)
            }
        }.store(in: &cancellables)
    }
    
    func recievedScroll(message: Message) {
        if message.direction == .down {
            let scroll = CGEvent(scrollWheelEvent2Source: nil, units: CGScrollEventUnit.line, wheelCount: 2, wheel1: Int32(-1), wheel2: -1, wheel3: 0)
            scroll?.post(tap: .cghidEventTap)
        } else if message.direction == .up {
        let scroll = CGEvent(scrollWheelEvent2Source: nil, units: CGScrollEventUnit.line, wheelCount: 2, wheel1: Int32(1), wheel2: 1, wheel3: 0)
        scroll?.post(tap: .cghidEventTap)
        }
    }
}

