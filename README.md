# iTrackPad

iTrackPad is an iOS and MacOs app that allows an iphone to be used as a trackpad to control a mac device.

## Installation

1) Clone into the repo and run the macOS project. 
2) Once the macOS project has been run an icon will appear on the status bar, click on that icon then click "start advertising"
3) Once advertising run the iOS project and the two apps should successfully pair.
4) Now use your iphone as a trackpad and enjoy!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)